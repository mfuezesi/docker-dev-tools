#
# Node.js w/ JSPM, Gulp and gems SASS, wordmove Dockerfile
#

# Pull base image.
FROM node:slim

MAINTAINER Melchior Wom Füzesi <mfuezesi@gmail.com>

RUN apt-get update
RUN apt-get install -y \
  apt-utils \
  ruby \
  rubygems-integration \
  rsync \
  git

# Clean up APT when done.
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN gem install \
  sass \
  wordmove

# Install Bower & Gulp
RUN npm install --save -g \
  gulp \
  jspm

# NPM cleanup
RUN npm cache clean

# Set env vars.
ENV NODE_ENV development

# Define working directory.
WORKDIR /home

# Define default command.
# CMD ["npm", "start"]
CMD ["/bin/bash"]

# Expose ports.
# Port for browser-sync UI.
EXPOSE 3001
