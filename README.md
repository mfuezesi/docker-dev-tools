## installs
* nodejs/npm latest
* gulpjs latest
* jspm latest
* sass latest
* wordmove latest
* rsync latest
* git latest

## settings
* NODE_ENV = development

## Don't forgett
* Add ```alias dt='docker run --rm -v `pwd`:/home dev-tools'``` and `docker` to `.zshrc`.
* Open ports to your docker host. (e.g. `coreos-vagrant`: `config.rb` with `$expose_docker_tcp=2375` and `$share_home=true`)
* Check out `https://www.jverdeyen.be/docker/discover-docker-dnsdock-coreos/` for dnsdock setup.
